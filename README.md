# Комплект базовых тестов проверки работоспособности изделия MX240 v5.x в комплектации с RTC v2.1

Комплект предназначен для проверки базовых функций изделия. Комплект представляет из себя загрузочную SD карту с комплектом необходимых программ. 

С помощью комплекта можно провести проверку следующих подсистем и устройств:

* светодиоды на лицевой панели
* работоспособность сетевых интерфейсов
* работоспособность USB
* работоспособность RS485
* работоспособность EEPROM
* работоспособность датчика температуры
* работоспособность часов реального времени
* работоспособность оперативной памяти
* исправность eMMC

## Описание тестов

### Светодиоды на лицевой панели

Требуется визуальный контроль оператора. 

Контроль светодиодов наличия питания и готовности не требует исполнения теста: контроль питания включается автоматически после подачи питания, 

После запуска теста исполнение приостанавливается, программа запрашивает готовность оператора произвести визуальный контроль исправности светодиодной индикации.

После подтверждения готовности тест начинается и продолжается примерно 10 секунд. Оператор должен наблюдать:

* светодиод "питание" - постоянно горит
* светодиод "готовность" - мигает примерно 1 рас в секунду
* светодиоды "передача RS485", 4 шт. - частое мигание (при условии комплектования изделия интерфейсами RS485)

После завершения теста оператору будет задан вопрос о том, наблюдал ли он все шесть светодиодов.

### Работоспособность сетевых интерфейсов

Проверяется возможность установления соединения интерфейсами eth0 и eth1, скорость соединения и прохождения пакетов.

Ожидается, что скорость соединения будет 1Гбит в секунду. Иная скорость считается ошибкой.

Отсутствие ответа на ping считается ошибкой.

#### Особенности тестирования

* Сразу после загрузки системы и после выполнения тестирования сетевые интерфейсы не активны.
* MAC адреса назначаются автоматически при активации интерфейсов.
* IP адреса назначаются автоматически при активации интерфейсов.
* Обнаружение установление соединения выполняется автоматически.
* Проверка прохождения пакетов выполняется стандартной утилитой ping на адрес 192.168.3.1 независимо от интерфейса. В тестовой среде должен находиться один ответчик с адресом 192.168.3.1.

### работоспособность USB

Работоспособность проверяется в конфигурациях USB2 или USB5.

Для проверки работоспособности в соответствующие разъемы на процессорной плате должны быть подключены шлейфами какие-либо USB устройства.

Тест считается успешным, если обнаружены подключенные к соответствующим разъемам устройства.

### Работоспособность RS485

Проверяется возможность передачи и приема каждым интерфейсом тестового файла. Тестовый файл передается поочередно на каждый интерфейс и на всех оставшихся выполняется прием. Каждый принятый файл сравнивается с переданным.

Тест считается пройденным, если все принятые файлы совпали с переданным образцом.

### Работоспособность EEPROM

*Деструктивный тест*: содержимое EEPROM может быть уничтожено в процессе тестирования.

В процессе тестирования:

* содержимое EEPROM сохраняется,
* EEPROM перезаписывается случайными данными
* содержимое EEPROM считывается и сравнивается с образцом. Тест считается успешным, если прочитанное содержимое EEPROM совпадает с образцом.
* сохраненное содержимое EEPROM записывается обратно, контроль не производится. 

### Работоспособность датчика температуры

Проверяется наличие и работоспособность внешнего по отношению к процессору датчика температуры. Ожидается, что датчик имеется на шине I2C, отвечает на запросы и сообщает корректную температуру.

### Работоспособность часов реального времени

Базовая проверка работоспособности часов реального времени при работе от основного источника питания.

Тест деструктивный: записанная в часы информация будет заменена.

В ходе тестирования:

* в часы записывается дата и время
* выдерживается установленная пауза
* из часов считывается время и выполняется сравнение с ожидаемым.

Тест считается пройденным, если ожидаемое время совпадает с прочитанным из часов.

### Работоспособность оперативной памяти

Сложный тест, который имеет свои особенности. 

Проверяется бОльшая часть оперативной памяти: часть памяти занята операционной системой и исполняемыми программами, часть памяти не доступна для тестирования по техническим причинам.

Тест выполняется автоматически, время выполнения значительное.

Тест после перезагрузки может быть выполнен однократно, последующие выполнения дадут сбой.

### Исправность eMMC

Проверяется устройство внутренней долговременной памяти eMMC на корректное выполнение операций записи/чтения.

Тест деструктивный: содержимое памяти eMMC будет перезаписано.

В процессе тестирования выполняется запись и чтение блоков данных, прочитанные данные сравниваются с записанным образцом.

Тест считается пройденным, если не был обнаружено ни одного сбойного блока.

В случае обнаружения сбойных блоков следует провести повторную проверку и принимать решение о возможности применения устройства в данной комплектации в индивидуальном порядке.

## Состав испытательного стенда

Испытательный стенд состоит из:

* что-то для подачи питания на проверяемые контроллеры. T-BUS в первозданном виде не подойдет: шина RS485 внутри него создаст помехи для проверяемых контроллеров и повлияет на результат
* сетевой коммутатор с количеством портов количеству на два больше максимального количества одновременно проверяемых контроллеров
* постоянно подключенный к коммутатору контроллер-ответчик с заданным адресом (192.168.3.1)
* патчкорды по количеству тестируемых портов
* блок питания соответствующей мощности
* сеть RS485 на четыре точки, индивидуальная на каждый тестируемый контроллер по количеству контроллеров. 
* загрузочная микро-SD карта с программой тестирования.

## Подготовка контроллера к тестированию

* собрать местную сеть RS485 на четыре точки в пределах контроллера
* вставить в слот загрузочную uSD карту с программой тестирования
* установить контроллер на шину питания
* подключить шлейфами ответчики на шине USB к соответствующим разъемам
* установить патчкордами соединить порты контроллера с портами коммутатора
* подключить консольный кабель к контроллеру

## Проведение тестирования

подать питание на контроллер
дождаться завершения загрузки
выполнить команду
mx240-test
ответить на вопросы 
после завершения тестирования на экран будет выведена таблица с результатами, проанализировать

## Управление тестированием

Программа поддерживает некоторое управление своим поведением с помощью ключей командной строки.

* -h справка по ключам командной строки
* -u2 подключить модуль тестирования конфигурации USB-2 (по умолчанию)
* -u5 подключить модуль тестирования конфигурации USB-5
* -n установить для всех вопросов ответ по умолчанию No. Применимо в тех случаях, когда нужно выполнить ограниченный набор тестов. Диалог выбора тестов будет выполняться.
* -y выполнить все тесты кроме теста светодиодов и теста конфигурации USB-5. Диалога не будет. Применимо для полностью автоматического тестирования.

Если программа вызывается без ключей в командной строке, то будет исполняться сценарий выбора тестов, тестирование светодиодов будет включено. По умолчанию будет исполняться тест USB-2.

Поведение программы и управление ее поведением могут быть изменены.

## Настройки

Файл настроек /usr/local/lib/mx240-test/00-settings

<pre>
# LEDS blinking time (s)
LED_TMO=${LED_TMO:-10}

# Ping responder address
PING_A1="192.168.3.1"

# First byte of the MAC address
PING_MAC_PREFIX="02:"
# IP address prefix from the same network with the responder
PING_APREFIX="192.168.3."
# Addressed from .1 to this are reserved and can't be assigned by this program
PING_ARESERVED="10"
# Address mask length
PING_AMASK="24"
# Timeout for waiting network connection (s)
TMO_CARRIER=60

# Memory size to test
MEMSIZE=450M

# USB ports and assotiated connectors
USB_PORT_U2=(1-1 2-1)
USB_CONN2=(X17 X18)
USB_PORT_U5=(1-1 2-1.1 2-1.2 2-1.3 2-1.4)
USB_CONN5=(X17 X18 X19 X20 X21)

# RS485 ports
RS485_PORTS="ttyM0 ttyM1 ttyM2 ttyM3"

# Time to sleep on the RTC test (s). Minimum of 10 s is recommended.
RTC_DELAY=30

# Acceptable thermal range for hwmon test
TEMP_LOW=15000
TEMP_HIGH=79999

# eMMC device name
EMMC_DEV=/dev/mmcblk1
# SHORT_EMMC=10
</pre>

## Отчет

После завершения работы программа выводит на экран отчет. По каждому тесту выводится информация
о состоянии теста:

* skip  - тест не исполнялся
* passed - тест завершился успешно
* failed - тест провален (возможна дополнительная расшифровка)

По некоторым тестав=м выводится дополнительная информация.

<pre>
============================================================
The final report
============================================================
            Front panel LED test : passed
                  Interface eth0 : passed
                  Interface eth1 : passed
   USB two ports version X17 X18 : failed
                                    
                                    X17 (1-1) - No device connected
                                    X18 (2-1) - No device connected
USB five ports version X17...X21 : failed
                                    
                                    X17 (1-1) - No device connected
                                    X18 (2-1.1) - No device connected
                                    X19 (2-1.2) - No device connected
                                    X20 (2-1.3) - No device connected
                                    X21 (2-1.4) - No device connected
                      RS485 test : passed
                                   ttyM0 ==>  N/A ok ok ok
                                   ttyM1 ==>  ok N/A ok ok
                                   ttyM2 ==>  ok ok N/A ok
                                   ttyM3 ==>  ok ok ok N/A
                     EEPROM test : passed
             Thermal sensor test : passed
                                    hwmon0/temp1_input 36.000deg '36000'
                        RTC test : passed
                     Memory test : passed
                       eMMC test : passed
============================================================
topaz:~#
</pre> 


# mx240-test



## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/plc5120833/mx240-test.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/plc5120833/mx240-test/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Set auto-merge](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!). Thank you to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Suggestions for a good README
Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
